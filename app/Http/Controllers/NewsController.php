<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\NewsTopicModel;
use Illuminate\Http\Request;
use Exception;

class NewsController extends Controller
{
    protected $request;
    protected $data;
    protected $header;

    function __construct(Request $request)
    {
        $this->request = $request;
        $this->data = $request->json()->all();
        $this->header = $request->header();
        $this->news = new News();
        $this->newsTopic = new NewsTopicModel();
    }

    public function createNews()
    {
        try{
            if(empty($this->data['topics'])){
                $response = $this->failedResponse('you should choose at least 1 topics','no topic selected');

            } else {
                $this->news->fill($this->data);
                $this->news->save();
                $message = 'success add news';
                $this->newsTopic->saveTopics($this->data['topics'],$this->news);
                $response = $this->successResponse($message,$this->news);
            }
        } catch (Exception $e) {
            $response = $this->failedResponse('failed add topics',$e->getMessage().' '.$e->getLine());
        }
        return $response;
    }

    public function newsDetail($id)
    {
        return $this->news->findNews($id);
    }

    public function updateNews()
    {
        $this->news = $this->news->findNews($this->data['id']);
        try{
            if(empty($this->data['topics'])){
                $response = $this->failedResponse('you should choose at least 1 topics','no topic selected');

            } else {
                $message = 'success update news';
                $this->newsTopic->updateTopics($this->data['topics'],$this->news);
                $this->news = $this->news->update($this->data);
                $response = $this->successResponse($message,$this->news);
            }
        } catch (Exception $e) {
            $response = $this->failedResponse('failed add topics',$e->getMessage().' '.$e->getLine().' '.$e->getFile());
        }
        return $response;
    }

    public function filterNews()
    {
        $data = $this->news->filter($this->data['topics_id'],$this->data['news_status']);
        $response = $this->successResponse('berhasil mencari berita',$data);
        return $response;
    }

}
