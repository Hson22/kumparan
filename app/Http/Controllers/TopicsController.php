<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;
use Exception;
class TopicsController extends Controller
{

    protected $request;
    protected $data;
    protected $header;

    function __construct(Request $request)
    {
        $this->request = $request;
        $this->data = $request->json()->all();
        $this->header = $request->header();
        $this->topics = new Topic();
    }

    public function createTopics()
    {
        try{
            $this->topics->fill($this->data);
            $this->topics->save();
            $message = 'success create topics';
            $response =  $this->successResponse($message,$this->topics);
        } catch (Exeption $e) {
            $response = $this->failedResponse('failed create_topics',$e->getMessage());
        }
        return $response;


    }

    public function getTopics()
    {
        try{
            $data = $this->topics->get();
            $message = 'success get topics';
            $response =  $this->successResponse($message,$data);
        } catch (Exeption $e) {
            $response = $this->failedResponse('failed get topics',$e->getMessage());
        }
        return $response;

    }
    public function updateTopics()
    {
        try{
            $this->topics = $this->topics->where('id',$this->data['id'])->first();
            $this->topics->topic_description = $this->data['topic_description'];
            $this->topics->save();
            $message = 'success update topics';
            $response =  $this->successResponse($message,$this->topics);
        } catch (Exeption $e) {
            $response = $this->failedResponse('failed update topics',$e->getMessage());
        }
        return $response;
    }

}
