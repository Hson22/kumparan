<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function successResponse($message,$data){
        $response['status'] = 'success';
        $response['message'] = $message;
        $response['data'] = $data;
        return $response;
    }

    public function failedResponse($message,$data){
        $response['status'] = 'failed';
        $response['message'] = $message;
        $response['data'] = $data;
        return $response;
    }
}
