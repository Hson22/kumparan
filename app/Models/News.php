<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $query;
    protected $table = 'news';
    protected $fillable = [
        'news_title',
        'news_status'
    ];


    public function findNews($id)
    {
        return $this->with('topics')->where('id',$id)->first();
    }

    public function topics()
    {
        return $this->belongsToMany('App\Models\Topic','news_to_topics','news_id','topics_id')->select(['id','topic_description']);
    }



    public function filterTopics($topics)
    {

        return $this;
    }
    public function filterStatus($status)
    {
         $this->query = $this->where('news_status',$status);
        return $this;
    }
    public function filter($topic,$status)
    {
        $data = $this;
        if($topic) {
           $data = $data->join('topics','topics.id','=','news.id')->where('topics.id',$topic);
        }
        if($status) {
            $data = $data->where('news_status',$status);
        }
        return $data->get();
    }
}
