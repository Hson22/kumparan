<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topics';
    protected $fillable = [
        'topic_description'
    ];


    protected $hidden = [
        'pivot'
    ];

    public function news()
    {
        return $this->belongsToMany('App\Models\News','news_to_topics','topics_id','news_id');
    }
}
