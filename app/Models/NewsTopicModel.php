<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsTopicModel extends Model
{
    protected $table = 'news_to_topics';
    protected $incrementals = false;
    protected $primaryKey = 'news_id';
    protected $fillable = [
        'news_id',
        'topics_id'
    ];

    public function saveTopics(array $topics,$news)
    {
        foreach($topics as $item)
        {
            $array['news_id'] = $news->id;
            $array['topics_id'] = $item;
            $this->create($array);
        }
    }

    public function updateTopics(array $topics,$news){
        $this->deleteTopics($news->id);
        $this->saveTopics($topics,$news);
    }

    public function deleteTopics($newsId)
    {
        $this->where('news_id',$newsId)->delete();
    }
}
