<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsToTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_to_topics', function (Blueprint $table) {
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('topics_id');
            $table->timestamps();
            $table->primary(['news_id','topics_id']);
            $table->foreign('news_id')->references('id')->on('news');
            $table->foreign('topics_id')->references('id')->on('topics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_to_topics');
    }
}
