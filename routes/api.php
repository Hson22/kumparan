<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'api','prefix'=>'news'],function() use($router){
    $router->post('/','NewsController@createNews');
    $router->get('/{id}','NewsController@newsDetail');
    $router->patch('/','NewsController@updateNews');
    $router->get('/','NewsController@filterNews');
});
Route::group(['middleware'=>'api','prefix'=>'topics'],function() use($router){
    $router->post('/','TopicsController@createTopics');
    $router->patch('/','TopicsController@updateTopics');
});